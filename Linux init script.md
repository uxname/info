# /etc/fstab
## For HDD:
`max_inline=256,autodefrag,compress=lzo,noatime,space_cache`

## For SSD:
`max_inline=256,ssd,compress=lzo,ssd_spread,noatime,space_cache`

## Improve swap
`sudo echo "vm.swappiness = 10" >> /etc/sysctl.conf && sudo sysctl -p`

### Add `fstrim` task to cron:

#### $ crontab -e
```
0 0 * * SUN sudo fstrim / && sudo fstrim /home && echo "Latest fstrim cron launch: $(date)" >> /home/__ENTER_USERNAME_HERE__/Desktop/fstrim.log
```
# After install
- ctrl+alt+L -> win+L
- reboot
- driver-manager
- timeshift-gtk

# .bash_aliases
```
alias apt='sudo apt'
alias upd='sudo apt update && sudo pkcon update && sudo snap refresh && flatpak update'
alias dcc='docker-compose'
```

# Xorg fix
```
sudo add-apt-repository ppa:nrbrtx/xorg-hotkeys
sudo apt-get update
```

# Install scripts
```bash
cd ~
mkdir Apps
cd ./Apps
sudo apt-get remove docker docker-engine docker.io

sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update
sudo apt-get -y upgrade

sudo apt-get -y install chkrootkit rkhunter bless golang-go gccgo zenmap pinta sirikali gocryptfs postgresql-client pgadmin3 keepass2 audacity kazam cpu-checker qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils ia32-libs-multiarch gnome-games pwgen adb gpp remmina remmina-plugin-rdp remmina-plugin-vnc virtualbox-ext-pack vlc inkscape yarn glogg gpick hardinfo virtualbox-qt nemo-seahorse sqlitebrowser ncdu pgmodeler docker-ce extlinux shutter umbrello cheese bleachbit mc htop gparted build-essential virtualbox oxygen-icon-theme kio snapd snapd-xdg-open libpcsclite-dev git

sudo adduser $USER kvm

sudo usermod -a -G docker $USER

sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/Release.key
sudo apt-key add Release.key
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ xenial main' !!!!!!!! IMPORTANT, RENAME xenial to artful FOR UBUNTU 18 / MINT 19
sudo apt-get update
sudo apt-get install --install-recommends winehq-stable

sudo snap install termius-app freemind redis-desktop-manager gravit-designer insomnia drawio

Add to `~/.bashrc` line: `alias dcc=docker-compose`

wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install node
nvm use node
n=$(which node);n=${n%/bin/node}; chmod -R 755 $n/bin/*; sudo cp -r $n/{bin,lib,share} /usr/local

```

# Jetbrains activation
https://plugins.zhile.io
