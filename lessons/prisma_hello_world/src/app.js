process.env.PRISMA_ENDPOINT = 'http://localhost:4466';

const prisma = require('./generated/prisma-client').prisma;

function log(data) {
    console.log(JSON.stringify(data, null, 2));
}

(async () => {
    log(await prisma.users());

    await prisma.createUser({
        email: "email@test.com",
        roles: {
            set: [
                'ADMIN',
                'MODERATOR'
            ]
        },
        password_hash: "123abc",
        password_salt: "123abc"
    });

    log(await prisma.users());

    const newUserIterator = await prisma.$subscribe.user().node();

    while (true) {
        const result = await newUserIterator.next();
        console.log('USER CHANGED: ', result);
    }
})();
