process.env.PRISMA_ENDPOINT = 'http://localhost:4466';

const prisma = require('./generated/prisma-client').prisma;
const {GraphQLServer} = require('graphql-yoga');
const {importSchema} = require('graphql-import');
const {rule, shield, and, or, not, allow, deny} = require('graphql-shield');

const resolvers = {
    Query: {
        activationCode: (root, args) => prisma.activationCode(args.where),
        activationCodes: (root, args) => prisma.activationCodes(args),

        category: (root, args) => prisma.category(args.where),
        categories: (root, args) => prisma.categories(args),

        product: (root, args) => prisma.product(args.where),
        products: (root, args) => prisma.products(args),

        user: (root, args) => prisma.user(args.where),
        users: (root, args) => prisma.users(args),
    },
    Mutation: {
        createActivationCode: (root, args) => prisma.createActivationCode(args.data),
        updateActivationCode: (root, args) => prisma.updateActivationCode(args),
        updateManyActivationCodes: (root, args) => prisma.updateManyActivationCodes(args),
        upsertActivationCode: (root, args) => prisma.upsertActivationCode(args),
        deleteActivationCode: (root, args) => prisma.deleteActivationCode(args.where),
        deleteManyActivationCodes: (root, args) => prisma.deleteManyActivationCodes(args.where),

        createCategory: (root, args) => prisma.createCategory(args.data),
        updateCategory: (root, args) => prisma.updateCategory(args),
        updateManyCategories: (root, args) => prisma.updateManyCategories(args),
        upsertCategory: (root, args) => prisma.upsertCategory(args),
        deleteCategory: (root, args) => prisma.deleteCategory(args.where),
        deleteManyCategories: (root, args) => prisma.deleteManyCategories(args.where),

        createProduct: (root, args) => prisma.createProduct(args.data),
        updateProduct: (root, args) => prisma.updateProduct(args),
        updateManyProducts: (root, args) => prisma.updateManyProducts(args),
        upsertProduct: (root, args) => prisma.upsertProduct(args),
        deleteProduct: (root, args) => prisma.deleteProduct(args.where),
        deleteManyProducts: (root, args) => prisma.deleteManyProducts(args.where),

        createUser: (root, args) => prisma.createUser(args.data),
        updateUser: (root, args) => prisma.updateUser(args),
        updateManyUsers: (root, args) => prisma.updateManyUsers(args),
        upsertUser: (root, args) => prisma.upsertUser(args),
        deleteUser: (root, args) => prisma.deleteUser(args.where),
        deleteManyUsers: (root, args) => prisma.deleteManyUsers(args.where),
    }
};

const isUser = rule({cache: 'no_cache'})(async (parent, args, ctx, info) => {
    return ctx.user !== null && ctx.user_type === 'user';
});

const isAdmin = rule({cache: 'no_cache'})(async (parent, args, ctx, info) => {
    return ctx.user !== null && ctx.user_type === 'admin';
});

const permissions = shield({
    Query: {
        activationCode: isAdmin,
        activationCodes: isAdmin,

        category: deny,
        categories: allow,

        product: isUser,
        products: isUser,

        user: allow,
        users: allow,
    },
    Mutation: {
        createActivationCode: allow,
        updateActivationCode: allow,
        updateManyActivationCodes: allow,
        upsertActivationCode: allow,
        deleteActivationCode: allow,
        deleteManyActivationCodes: allow,

        createCategory: allow,
        updateCategory: allow,
        updateManyCategories: allow,
        upsertCategory: allow,
        deleteCategory: allow,
        deleteManyCategories: allow,

        createProduct: allow,
        updateProduct: allow,
        updateManyProducts: allow,
        upsertProduct: allow,
        deleteProduct: allow,
        deleteManyProducts: allow,

        createUser: allow,
        updateUser: allow,
        updateManyUsers: allow,
        upsertUser: allow,
        deleteUser: allow,
        deleteManyUsers: allow,
    }
});

const server = new GraphQLServer({
    typeDefs: importSchema(__dirname + '/schema/schema.graphql'),
    resolvers: resolvers,
    middlewares: [permissions],
    context: async ({request}) => {
        console.log('Requested user type: ', request.headers.user_type);
        return {
            user_type: request.headers.user_type
        }
    }
});

server.start({
    endpoint: '/graphql',
    playground: '/playground',
    port: 4000
});
