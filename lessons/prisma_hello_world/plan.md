* Запуск окружения
* Обзор структуры файлов
* PGAdmin
* Prisma:
  * deploy
  * reset
  * seed
  * export
  * import
  * playground
  * prisma graphql sdl
    * migrations
    * relations
  * introspection
* Prisma client
* GraphQL API с помощью prisma-client
