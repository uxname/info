let database = [
    {
        id: "1",
        username: "user_1",
        age: 10
    },
    {
        id: "2",
        username: "user_2",
        age: 20
    },
    {
        id: "3",
        username: "user_3",
        age: 30
    }
];

const resolvers = {
    Query: {
        echo: ((root, args, context, info) => {
            const {text} = args;
            return text;
        }),
        users: ((root, args, context, info) => {
            return database;
        })
    },
    Mutation: {
        createUser: ((root, args, context, info) => {
            const id = Math.random().toString();
            const newUser = {
                id: id,
                username: args.user.username,
                age: args.user.age
            };

            database.push(newUser);
            return newUser;
        }),
        deleteUser: ((root, args, context, info) => {
            const {userId} = args;

            const dbSizeBeforeDelete = database.length;
            database = database.filter(item => {
                return item.id !== userId;
            });
            const dbSizeAfterDelete = database.length;

            if (dbSizeBeforeDelete !== dbSizeAfterDelete) {
                return true;
            } else {
                throw new Error('User not found');
            }

        })
    }
};


module.exports = resolvers;
