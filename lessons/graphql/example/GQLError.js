const BaseGraphQLError = require('graphql').GraphQLError;

class GQLError extends BaseGraphQLError {

    constructor({message, code, data, internalData}) {
        super({
            message: message || 'Unknown error',
            code: code || -1,
            data: data,
            internalData: internalData,
            IS_GQLERROR: true
        });
    }

    static formatError(error) {
        if (error.message.IS_GQLERROR) {
            return {
                message: error.message.message
            }
        }
    }
}
