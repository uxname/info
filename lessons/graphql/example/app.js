const {GraphQLServer} = require('graphql-yoga');
const {importSchema} = require('graphql-import');

const server = new GraphQLServer({
    typeDefs: importSchema('./schema/schema.graphql'),
    resolvers: require('./resolvers'),
    mocks: true
});

server.start({
  endpoint: "/gql",
  playground: "/gql",
  port: 4400
});
