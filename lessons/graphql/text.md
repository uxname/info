![](./images/graphql.png)

# Основные понятия

- GraphQL - это формат взаимодействия клиента и сервера
- Аналог SQL для фронтенда
- Общение (обычно) происходит поверх HTTP, через POST запросы
- Коды ответа: **200** (OK) / **500** (ERROR)
- Основные фреймворки для JS: Apollo, Yoga, Graphql.js
- Subscriptions
- GraphQL SDL (Schema Definition Language)

---

## Пример SDL:

- type
- field
- enum
- interface
- directive [ depercated, cacheControl, permissions, default values ]

---

```graphql
type Post implements Item {
  id: ID!
  title: String!
  publishedAt: DateTime!
  likes: Int! @default(value: 0)
  blog: Blog
}

type Blog {
  id: String!
  name: String!
  description: String
  category: Category!
}

enum Category {
  PROGRAMMING_LANGUAGES
  API_DESIGN
}

interface Item {
  title: String!
}
```

## Scalar types

- Int — целое число
- Float — число с плавающей точкой
- String — строка в формате UTF-8
- Boolean — true/false
- ID — строка; уникальный идентификатор

## Custom scalar types

- Не хватает 5 скалярных типов?
- Хочется сразу работать с Date, а не циферками?
- Date, Email, URL, LimitedString, Password, SmallInt ...
- Придется реализовать функцию сериализации/десериализации объекта в текст.

- В GraphQL Yoga есть дополнительные скалярные типы, например `DateTime` и `Json`

## Playground

- Query panel
- Headers panel
- Variables panel
- Schema explorer
- Tracing panel

**Query**
![](./images/playground_query.png)

**Mutation**
![](./images/playground_mutation.png)

**Headers**
![](./images/playground_headers.png)

**Variables**
![](./images/playground_variables.png)

**Schema explorer**
![](./images/playground_explorer.png)

# GraphQL со стороны фронтэнда

Основные GraphQL клиенты:

- `apollo-client`
- `graphql-request`

Пример на `graphql-request`

```js
import { request } from "graphql-request";

const query = `{
  Movie(title: "Inception") {
    releaseDate
    actors {
      name
    }
  }
}`;

request("https://api.graph.cool/simple/v1/movies", query).then(data =>
    console.log(data)
);
```

**Пример сложного запроса:**

```graphql
{
  user(where: { email: "admin@admin.com" }) {
    id
    email
    roles
  }
  categories {
    id
    title
    products(where: { title_contains: "cat" }) {
      id
      title
      description
      categories {
        id
        products {
          id
          title
        }
      }
    }
  }
}
```

# Hello world сервер на GraphQL Yoga:

```js
const { GraphQLServer } = require("graphql-yoga");

const typeDefs = `
  type Query {
    hello(name: String): String!
  }
`;

const resolvers = {
  Query: {
    hello: (root, args, context, info) => {
      return `Hello ${args.name || "World"}`;
    }
  }
};

const server = new GraphQLServer({
  typeDefs: typeDefs,
  resolvers: resolvers
});

server.start({
  endpoint: "/graphql",
  playground: "/graphql",
  port: 4000
});
```

- **root** — объект с данными, который был получен от resolve метода родительского поля.
- **args** — содержит провалидированные значения аргументов для поля, которые передал клиент в своем запросе.
- **context** — это ваш глобальный объект, который создается для каждого GraphQL-запроса и доступен во всех resolve методах
- **info** — содержит служебную информацию о текущем поле, пути в графе, GraphQL-схеме, выполняемом GraphQL-запросе и переменных.

# Вложенные запросы:

- Вложенные запросы

```js
const { GraphQLServer } = require("graphql-yoga");

const typeDefs = `
  type Query {
    users: [User]
  }

  type User {
      id: ID
      name: String
      age: Int
      pc: PC
  }

  type PC {
      cpu: String
      memory: Int
  }
`;

const resolvers = {
  Query: {
    users: (root, args, context, info) => {
      return [
        {
          id: 1,
          name: "Test user"
          // skip 'age' and 'pc' fields
        },
        {
          id: 2,
          name: "Test user #2"
          // skip 'age' and 'pc' fields
        }
      ];
    }
  },
  User: {
    age: (root, args, context, info) => {
      // root === current User
      return db.getUser(root.id).age;
    },
    pc: (root, args, context, info) => {
      // root === current User
      const pc = db.getUserPc(root.id);
      return {
        cpu: pc.cpu
        // skip memory field
      };
    }
  },
  PC: {
    memory: (root, args, context, info) => {
      // root === current PC
      return 16;
    }
  }
};

const server = new GraphQLServer({
  typeDefs: typeDefs,
  resolvers: resolvers
});

server.start({
  endpoint: "/graphql",
  playground: "/graphql",
  port: 4000
});
```

# Query выполняются параллельно, Mutation последовательно

**Query**

```graphql
{
  user(id: 55) {
    id
    description
  }
  post(id: 12) {
    content
    date
  }
}
```

**Mutation**

```graphql
mutation {
  createPost(input: { content: "Hello world", authorId: 1 }) {
    id
  }
  createUser(input: { username: "user", password: "123" }) {
    id
  }
}
```

# Много input параметров = один объект

Когда входящих параметров может быть много:

**Лучше:**

```js
const typeDefs = `
  type Query {
    users(where: UsersWhereInput): [User]
  }

  type User {
      id: ID
      name: String
      age: Int
  }

  type UsersWhereInput {
      id: ID
      name: String
  }
`;
```

```graphql
{
  users(where: { id: 123 }) {
    id
    name
    age
  }
}
```

**Хуже:**

```js
const typeDefs = `
  type Query {
    users(id: ID, name: String): [User]
  }

  type User {
      id: ID
      name: String
      age: Int
  }
`;
```

```graphql
{
  users(id: 123) {
    id
    name
    age
  }
}
```

# Доступ к express

```js
const graphqlServer = new GraphQLServer({
  typeDefs: schema,
  resolvers: resolvers
});

graphqlServer.express.get("/", (req, res) => {
  res.status(404).json({
    message: "Please use /graphql endpoint"
  });
});

graphqlServer.express.use((req, res, next) => {
  const ip = req.connection.remoteAddress;
  console.log(`HTTP request from: ${ip}`);
  next();
});
```

# Mocks

```js
const typeDefs = `
  type Query {
    users(id: ID, name: String): [User]
    user(id: ID): User
    randomText: String
    randomInt: Int
  }

  type User {
      id: ID
      name: String
      age: Int
  }
`;

const mocks = {
  Int: () => {
    return randomInt(1, 100);
  },
  String: () => {
    return `Hello world`;
  },
  User: {
    id: () => {
      return 99;
    }
  }
};

const graphqlServer = new GraphQLServer({
  mocks: mocks, // boolean or object
  typeDefs: typeDefs,
  resolvers: null
});
```

# Context

Context это объект с данными текущего пользователя и глобальными ресурсами, которые будут доступны в резолверах вашей GraphQL-схемы

**HTTP server -> GraphQL server -> Context function -> resolvers**

```js
const resolvers = {
  Query: {
    users: (root, args, context, info) => {
      const user = context.user;
      const token = context.token;
      const req = context.req;

      console.log(useid);

      return [];
    }
  }
};

const graphqlServer = new GraphQLServer({
  typeDefs: typeDefs,
  resolvers: resolvers,
  context: async ({ request }) => {
    let user = null;
    try {
      user = await jwt.validateToken(request.headers.token);
    } catch (e) {
      log.trace("Can't get user from token:", e.message);
    }

    return {
      user: user,
      token: request.headers.token,
      req: request
    };
  }
});
```

# Documentation comments

```graphql
const typeDefs = `
  type Query {
    """Get users by names"""
    users(nameContain: String): [User]

    """Get user by id"""
    user(id: ID): User

    """Get random text"""
    randomText: String
    randomInt: Int
  }

  """
  Multi
  line
  description of
  User entity
  """
  type User {
      id: ID
      name: String
      age: Int
  }
`;
```

# Обработка ошибок

В GraphQL я бы выделил 3 группы ошибок:

- **Фатальные ошибки** `(500 Internal Server Error)`
  - кончилась память
  - забыли установить пакет
  - грубая синтаксическая ошибка в коде
  - ошибки сервера
- **Ошибки валидации** `(graphql)`
  - ошибка невалидного GraphQL-запроса
  - не передали переменную
  - проверяет на соответствие с вашей GraphQL-схемой
    - запросили несуществующее поле
    - не передали обязательный аргумент
    - передали неверный тип для аргумента
- **Пользовательские ошибки** `(в резолверах)`
  - throw new Error("")
  - ошибка невалидного значения в return

GraphQL возвращает массив ошибок, ведь в одном запросе может быть много ошибок.

```js
const { GraphQLServer } = require("graphql-yoga");

const typeDefs = `
  type Query {
    hello(name: String): String!
  }
`;

const resolvers = ...

const server = new GraphQLServer({
  typeDefs: typeDefs,
  resolvers: resolvers
});

server.start({
  endpoint: "/graphql",
  playground: "/graphql",
  port: 4000
});
```

- NULL на не-NULL типе (`String!`)

```js
const resolvers = {
  Query: {
    hello: (root, args, context, info) => {
      return null; // Error: return null on not-null field
    }
  }
};
```

- Throw

```js
const resolvers = {
  Query: {
    hello: (root, args, context, info) => {
      if (true) {
        throw new Error("error message");
      }
      return "Hello";
    }
  }
};
```

- Без return

```js
const resolvers = {
  Query: {
    hello: (root, args, context, info) => {
      const a = 1;
      const b = 1;
      const c = a + b;
      // Error: return undefined on not-null field
    }
  }
};
```

Два запроса, один из них с ошибкой:

Запрос:

```graphql
{
  posts {
    nodes {
      id
    }
  }
  mediaItems(first: -1) {
    nodes {
      id
    }
  }
}
```

Ответ:

```json
{
  "data": {
    "posts": {
      "nodes": [
        {
          "id": "cG9zdDox"
        }
      ]
    },
    "mediaItems": null
  },
  "errors": [
    {
      "message": "first must be a positive intege",
      "category": "user",
      "locations": [
        {
          "line": 7,
          "column": 3
        }
      ],
      "path": ["mediaItems"]
    }
  ]
}
```

# Свой формат ошибок

Для отправки error объектов своего формата - нужно:
переопределить функцию `formatError` при старте Graphql-yoga сервера.

```js
formatError: error => {
  return {
    message: error.message,
    code: 100,
    comment: "Just an error",
    additionalData: {
      date: Date.now()
    }
  };
};
```

# Async resolvers

```js
const resolvers = {
  Query: {
    hello: async (root, args, context, info) => {
      const result = await getRandomTextWithDelay("text", 1000);

      return result;
    }
  }
};
```

# Загрузка файлов через GraphQL

Никак.

На самом деле можно, но неудобно, лучше делать через REST API.

Плюсы загрузки через REST:

- есть уже куча инструментов и библиотек
- догрузка файлов
- отображение процесса загрузки
- разделение серверов по ролям (легче админить)
- файловые-сервера
- graphql-сервера

Либо через поле с Base64, минусы такого подхода:

- как минимум +30% в размере передаваемых данных по сети
- кушаем процессор как на клиенте так и на сервере
- тратим много дорогой памяти

# Must have библиотеки

- **graphql-cost-analysis** - ограничение размера запросов (защита от рекурсий)
- **graphql-import** - возможность использования функции `#import ...` в `*.graphql` файлах. Возможность разнесения схемы по нескольким файлам.
- **graphql-shield** - Разграничение прав доступа к объектам схемы.

# Кратко о Prisma:

## Prisma - это

Prisma - это ORM к базам данных PostgreSQL/MySQL/MongoDB, предоставляющая GraphQL API для доступа к БД.
Это позволяет облегчить работу с БД а также создавать резолверы полностью копирующие резолверы призмы, например:

```js
const resolvers = {
    Query: {
        address: (root, {where}) => prisma.address(where),
        addresses: (root, args) => prisma.addresses(args),

        category: (root, {where}) => prisma.category(where),
        categories: (root, args) => prisma.categories(args),

        currency: (root, {where}) => prisma.currency(where),
        currencies: (root, args) => prisma.currencies(args),
        ...
    }
}
```

А для защиты/разграничения прав использовать какой-нибудь GraphQL middleware, например `graphql-shield`:

```js
const permissions = shield({
        Query: {
            address: and(isAuthenticated, isAdmin),
            addresses: and(isAuthenticated, isAdmin),

            category: allow,
            categories: allow,

            currency: allow,
            currencies: allow,

            users: isAdmin
        },
        Store: {
            address: allow,
            invoices: and(isAuthenticated, or(isAdmin, isModerator, isSellerOfThisStore)),
            products: allow,
            title: isAuthenticated,
            featured_products: allow,
            sellers: allow,
            followers: allow
        }
        ...
});
```

# Contact me:

- Telegram: [@uxname](https://t.me/uxname)
- Email: uxname@gmail.com
